(self["webpackChunktunel_do_amor"] = self["webpackChunktunel_do_amor"] || []).push([["src_app_pages_chat-box_chat-box_module_ts"],{

/***/ 4781:
/*!**********************************************************************!*\
  !*** ./node_modules/ngx-auto-scroll/__ivy_ngcc__/ngx-auto-scroll.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NgxAutoScroll": () => (/* binding */ NgxAutoScroll),
/* harmony export */   "NgxAutoScrollModule": () => (/* binding */ NgxAutoScrollModule)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 7716);



class NgxAutoScroll {
    /**
     * @param {?} element
     */
    constructor(element) {
        this.lockYOffset = 10;
        this.observeAttributes = "false";
        this._isLocked = false;
        this.nativeElement = element.nativeElement;
    }
    /**
     * @return {?}
     */
    getObserveAttributes() {
        return this.observeAttributes !== "" && this.observeAttributes.toLowerCase() !== "false";
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.mutationObserver = new MutationObserver(() => {
            if (!this._isLocked) {
                this.scrollDown();
            }
        });
        this.mutationObserver.observe(this.nativeElement, {
            childList: true,
            subtree: true,
            attributes: this.getObserveAttributes(),
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.mutationObserver.disconnect();
    }
    /**
     * @return {?}
     */
    forceScrollDown() {
        this.scrollDown();
    }
    /**
     * @return {?}
     */
    isLocked() {
        return this._isLocked;
    }
    /**
     * @return {?}
     */
    scrollDown() {
        this.nativeElement.scrollTop = this.nativeElement.scrollHeight;
    }
    /**
     * @return {?}
     */
    scrollHandler() {
        const /** @type {?} */ scrollFromBottom = this.nativeElement.scrollHeight - this.nativeElement.scrollTop - this.nativeElement.clientHeight;
        this._isLocked = scrollFromBottom > this.lockYOffset;
    }
}
NgxAutoScroll.ɵfac = function NgxAutoScroll_Factory(t) { return new (t || NgxAutoScroll)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef)); };
NgxAutoScroll.ɵdir = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({ type: NgxAutoScroll, selectors: [["", "ngx-auto-scroll", ""]], hostBindings: function NgxAutoScroll_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function NgxAutoScroll_scroll_HostBindingHandler() { return ctx.scrollHandler(); });
    } }, inputs: { lockYOffset: ["lock-y-offset", "lockYOffset"], observeAttributes: ["observe-attributes", "observeAttributes"] } });
/**
 * @nocollapse
 */
NgxAutoScroll.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef, },
];
NgxAutoScroll.propDecorators = {
    'lockYOffset': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input, args: ["lock-y-offset",] },],
    'observeAttributes': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input, args: ["observe-attributes",] },],
    'scrollHandler': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ["scroll",] },],
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxAutoScroll, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive,
        args: [{
                selector: "[ngx-auto-scroll]"
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef }]; }, { lockYOffset: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input,
            args: ["lock-y-offset"]
        }], observeAttributes: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input,
            args: ["observe-attributes"]
        }], 
    /**
     * @return {?}
     */
    scrollHandler: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener,
            args: ["scroll"]
        }] }); })();

class NgxAutoScrollModule {
}
NgxAutoScrollModule.ɵfac = function NgxAutoScrollModule_Factory(t) { return new (t || NgxAutoScrollModule)(); };
NgxAutoScrollModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NgxAutoScrollModule });
NgxAutoScrollModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[]] });
/**
 * @nocollapse
 */
NgxAutoScrollModule.ctorParameters = () => [];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxAutoScrollModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                declarations: [NgxAutoScroll],
                imports: [],
                exports: [NgxAutoScroll]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](NgxAutoScrollModule, { declarations: [NgxAutoScroll], exports: [NgxAutoScroll] }); })();

/**
 * Generated bundle index. Do not edit.
 */



//# sourceMappingURL=ngx-auto-scroll.js.map

/***/ }),

/***/ 5890:
/*!***********************************************************!*\
  !*** ./src/app/pages/chat-box/chat-box-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChatBoxPageRoutingModule": () => (/* binding */ ChatBoxPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _chat_box_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chat-box.page */ 2680);




const routes = [
    {
        path: '',
        component: _chat_box_page__WEBPACK_IMPORTED_MODULE_0__.ChatBoxPage
    }
];
let ChatBoxPageRoutingModule = class ChatBoxPageRoutingModule {
};
ChatBoxPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ChatBoxPageRoutingModule);



/***/ }),

/***/ 6222:
/*!***************************************************!*\
  !*** ./src/app/pages/chat-box/chat-box.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChatBoxPageModule": () => (/* binding */ ChatBoxPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _chat_box_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chat-box-routing.module */ 5890);
/* harmony import */ var _chat_box_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./chat-box.page */ 2680);
/* harmony import */ var ngx_auto_scroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-auto-scroll */ 4781);









let ChatBoxPageModule = class ChatBoxPageModule {
};
ChatBoxPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _chat_box_routing_module__WEBPACK_IMPORTED_MODULE_0__.ChatBoxPageRoutingModule,
            ngx_auto_scroll__WEBPACK_IMPORTED_MODULE_2__.NgxAutoScrollModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.ReactiveFormsModule
        ],
        declarations: [_chat_box_page__WEBPACK_IMPORTED_MODULE_1__.ChatBoxPage],
    })
], ChatBoxPageModule);



/***/ }),

/***/ 2680:
/*!*************************************************!*\
  !*** ./src/app/pages/chat-box/chat-box.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChatBoxPage": () => (/* binding */ ChatBoxPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_chat_box_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./chat-box.page.html */ 4065);
/* harmony import */ var _chat_box_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./chat-box.page.scss */ 8244);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-socket-io */ 5421);







// import { FileServiceService } from 'src/app/services/file-service.service';
let ChatBoxPage = class ChatBoxPage {
    constructor(socket, httpClient, route) {
        this.socket = socket;
        this.httpClient = httpClient;
        this.route = route;
        this.ACTIONS = {
            MESSAGE: 'message',
            START_TYPING: 'start',
            STOP_TYPING: 'stop',
            ALL_MESSAGES: 'all-messages'
        };
        this.fileToUpload = null;
        this.messages = [];
        this.currentMessage = "";
        this.myIdx = 1;
        this.users = [{
                name: "",
                image: '../../../assets/user.png'
            },
            {
                name: "",
                image: '../../../assets/user.png'
            }];
    }
    sendMsg() {
        let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`;
        let message = {
            text: this.currentMessage,
            sender: this.myIdx,
            time: time,
            type: "text",
        };
        this.socket.emit(this.ACTIONS.MESSAGE, message);
        this.currentMessage = '';
    }
    proccesMessage(message) {
        this.messages.push(message);
    }
    hasGetUserMedia() {
        return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
    }
    handleFileInput(files) {
        this.fileToUpload = files.item(0);
        console.log(this.fileToUpload);
        console.log('this.fileToUpload');
        this.httpClient.post("/archives", { name: 'teste' }).subscribe((res) => {
            let formData = new FormData();
            formData.append('data', this.fileToUpload, this.fileToUpload.name);
            let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpHeaders();
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            let nome = res.id;
            this.httpClient.post(`/archives/${nome}/file`, formData, { headers: headers }).subscribe((res) => {
                let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`;
                let message = {
                    text: nome + '.png',
                    sender: this.myIdx,
                    time: time,
                    type: "image",
                };
                this.socket.emit(this.ACTIONS.MESSAGE, message);
                this.currentMessage = '';
            });
            console.log("request feito?????");
        });
        // this.fileService.createFileModel().subscribe((res)=>{
        //   console.log('criou???');
        //   console.log(res);
        // })
    }
    ngOnInit() {
        if (this.hasGetUserMedia()) {
            console.log("aeeee");
            // Good to go!
        }
        else {
            alert("getUserMedia() is not supported by your browser");
        }
        this.route.queryParams.subscribe(params => {
            this.myIdx = params['idx'];
        });
        this.socket.connect();
        this.socket.on(this.ACTIONS.MESSAGE, (msg) => {
            this.proccesMessage(msg);
        });
        this.socket.on(this.ACTIONS.ALL_MESSAGES, (msgs) => {
            this.messages = msgs;
        });
    }
};
ChatBoxPage.ctorParameters = () => [
    { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_3__.Socket },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.ActivatedRoute }
];
ChatBoxPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-chat-box',
        template: _raw_loader_chat_box_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_chat_box_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ChatBoxPage);



/***/ }),

/***/ 8244:
/*!***************************************************!*\
  !*** ./src/app/pages/chat-box/chat-box.page.scss ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGF0LWJveC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 4065:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/chat-box/chat-box.page.html ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>chat-box</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"chat\">\n    <div class=\"card\">\n      <div class=\"card-header msg_head\">\n        <div class=\"d-flex bd-highlight\">\n          <div class=\"img_cont\">\n            <img src=\"../../../assets/user.png\" class=\"rounded-circle user_img\">\n            <span class=\"online_icon\"></span>\n          </div>\n          <div class=\"user_info\">\n            <span>Chat with Khalid</span>\n            <p>1767 Messages</p>\n          </div>\n          <div class=\"video_cam\">\n            <span><i class=\"fas fa-video\"></i></span>\n            <span><i class=\"fas fa-phone\"></i></span>\n          </div>\n        </div>\n        <span id=\"action_menu_btn\"><i class=\"fas fa-ellipsis-v\"></i></span>\n        <div class=\"action_menu\">\n          <ul>\n            <li><i class=\"fas fa-user-circle\"></i> View profile</li>\n            <li><i class=\"fas fa-users\"></i> Add to close friends</li>\n            <li><i class=\"fas fa-plus\"></i> Add to group</li>\n            <li><i class=\"fas fa-ban\"></i> Block</li>\n          </ul>\n        </div>\n      </div>\n\n      <div class=\"card-body msg_card_body\" ngx-auto-scroll>\n        <ng-container *ngFor=\"let message of messages\">\n          <div class=\"d-flex mb-4\"\n            [ngClass]=\"{'justify-content-start':message.sender!=myIdx,'justify-content-end':message.sender==myIdx}\">\n            <div class=\"img_cont_msg\" *ngIf=\"message.sender!=myIdx\">\n              <img [src]=\"users[message.sender].image\" class=\"rounded-circle user_img_msg\">\n            </div>\n            <ng-container *ngIf=\"message.type=='text'\">\n              <div [ngClass]=\"{'msg_cotainer':message.sender!=myIdx,'msg_cotainer_send':message.sender==myIdx}\"\n                [innerText]=\"message.text\">\n                <span class=\"msg_time\">{{message.time}}</span>\n              </div>\n            </ng-container>\n            <ng-container *ngIf=\"message.type=='image'\">\n            <div [ngClass]=\"{'msg_cotainer':message.sender!=myIdx,'msg_cotainer_send':message.sender==myIdx}\">\n              <img [src]=\"message.text\">\n              <span class=\"msg_time\">{{message.time}}</span>\n            </div>\n          </ng-container>\n            <div class=\"img_cont_msg\" *ngIf=\"message.sender==myIdx\">\n              <img [src]=\"users[message.sender].image\" class=\"rounded-circle user_img_msg\">\n            </div>\n          </div>\n        </ng-container>\n      </div>\n\n      <div class=\"card-footer\">\n        <div class=\"input-group\">\n          <div class=\"input-group-append\">\n            <label for=\"file\" class=\"input-group-text attach_btn\">\n              <span><i class=\"fas fa-paperclip\"></i></span>\n\n            </label>\n            <input type=\"file\" name=\"file\" id=\"file\" (change)=\"handleFileInput($event.target.files)\">\n\n          </div>\n          <textarea name=\"\" class=\"form-control type_msg\" [(ngModel)]=\"currentMessage\"\n            placeholder=\"Type your message...\"></textarea>\n          <div class=\"input-group-append\" (click)=\"sendMsg()\">\n            <span class=\"input-group-text send_btn\"><i class=\"fas fa-location-arrow\"></i></span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_chat-box_chat-box_module_ts.js.map