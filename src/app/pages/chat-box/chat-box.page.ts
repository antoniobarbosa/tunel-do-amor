import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { environment } from '../../../environments/environment';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { ModalController } from '@ionic/angular';
// import { Media, MediaObject } from '@ionic-native/media';
// import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { NgxAutoScroll } from 'ngx-auto-scroll';
// import { FileServiceService } from 'src/app/services/file-service.service';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.page.html',
  styleUrls: ['./chat-box.page.scss'],
})
export class ChatBoxPage implements OnInit {
  @ViewChild(NgxAutoScroll) ngxAutoScroll: NgxAutoScroll;
  private ACTIONS = {
    MESSAGE: 'message',
    START_TYPING: 'start',
    STOP_TYPING: 'stop',
    ALL_MESSAGES: 'all-messages'
  }
  constructor(private transfer: FileTransfer, private mediaCapture: MediaCapture, private socket: Socket, public httpClient: HttpClient, private route: ActivatedRoute, public modalController: ModalController, private media: Media) {

  }
  fileTransfer: FileTransferObject = this.transfer.create();

  audioFile: MediaObject
  fileToUpload: File | null = null;

  private messages = [
  ];
  private currentMessage = "";
  private myIdx = 1;

  users = [{
    name: "",
    image: '../../../assets/user.png'
  },
  {
    name: "",
    image: '../../../assets/user.png'
  }]


  startRecording() {
    this.mediaCapture.captureAudio().then(
      // (data: MediaFile[]) => {
      //   this.httpClient.post<any>(`${environment.endpont}/archives`, { name: 'teste' }).subscribe((res) => {
      //     let nome = res.id;
      //     let options: FileUploadOptions = {
      //       fileKey: 'ionicfile',
      //       fileName: 'ionicfile',
      //       chunkedMode: false,
      //       mimeType: "audio",
      //       headers: {}
      //     }
      //     this.fileTransfer.upload(data[0].fullPath, `${environment.endpont}/archives/${nome}/file`, options)
      //       .then((data) => {
      //         console.log(data + " Uploaded Successfully");
      //       });

      //     let formData: FormData = new FormData();
      //     alert(data[0].fullPath);
      //     alert(data[0].fullPath.substr(0, data[0].fullPath.lastIndexOf('/')));
      //     this.file.resolveDirectoryUrl(data[0].fullPath.substr(0, data[0].fullPath.lastIndexOf('/'))).then((rootDir) => {
      //       console.log("testee");
      //       console.log(rootDir);
      //       return this.file.getFile(rootDir, data[0].name, { create: false })
      //     })
      //       .then((fileEntry) => {              
      //         fileEntry.file(file => {               
      //           let headers = new HttpHeaders();
      //           headers.append('Content-Type', 'multipart/form-data');
      //           headers.append('Accept', 'application/json');
      //           let nome = res.id;
      //           this.httpClient.post(`${environment.endpont}/archives/${nome}/file`, formData, { headers: headers }).subscribe((res) => {
      //             let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`
      //             let message = {
      //               text: `${environment.endpont}/images/${nome}.wav`,
      //               sender: this.myIdx,
      //               time: time,
      //               type: "audio",
      //             };
      //             this.socket.emit(this.ACTIONS.MESSAGE, message);
      //             this.currentMessage = '';
      //           })
      //         })
      //       })
      //   });
      // },
      // (err: CaptureError) => console.error(err)
    );
  }


  sendMsg() {
    let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`
    let message = {
      text: this.currentMessage,
      sender: this.myIdx,
      time: time,
      type: "text",
    };
    this.socket.emit(this.ACTIONS.MESSAGE, message);
    this.currentMessage = '';

  }

  proccesMessage(message) {
    this.messages.push(message);
    setTimeout(() => { this.ngxAutoScroll.forceScrollDown() }, 200);
  }
  hasGetUserMedia() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    console.log('this.fileToUpload');

    this.httpClient.post<any>(`${environment.endpont}/archives`, { name: 'teste' }).subscribe((res) => {
      let formData: FormData = new FormData();
      formData.append('data', this.fileToUpload, this.fileToUpload.name);
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      let nome = res.id;
      this.httpClient.post(`${environment.endpont}/archives/${nome}/file`, formData, { headers: headers }).subscribe((res) => {
        let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`
        let message = {
          text: `${environment.endpont}/images/${nome}.png`,
          sender: this.myIdx,
          time: time,
          type: "image",
        };
        this.socket.emit(this.ACTIONS.MESSAGE, message);
        this.currentMessage = '';
      })
    });
  }
  handleAudioFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    console.log('this.fileToUpload');

    this.httpClient.post<any>(`${environment.endpont}/archives`, { name: 'teste' }).subscribe((res) => {
      let formData: FormData = new FormData();
      formData.append('data', this.fileToUpload, this.fileToUpload.name);
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      let nome = res.id;
      this.httpClient.post(`${environment.endpont}/archives/${nome}/file`, formData, { headers: headers }).subscribe((res) => {
        let time = `${new Date().getHours()}:${new Date().getMinutes() > 9 ? new Date().getMinutes() : '0' + new Date().getMinutes()}`
        let message = {
          text: `${environment.endpont}/images/${nome}.wav`,
          sender: this.myIdx,
          time: time,
          type: "audio",
        };
        this.socket.emit(this.ACTIONS.MESSAGE, message);
        this.currentMessage = '';
      })
    });
  }
  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.myIdx = params['idx'] ? params['idx'] : 0;
    });

    this.socket.connect();
    this.socket.on(this.ACTIONS.MESSAGE, (msg) => {
      this.proccesMessage(msg);
    })
    this.socket.on(this.ACTIONS.ALL_MESSAGES, (msgs) => {
      console.log(JSON.stringify(msgs));
      this.messages = msgs;
    });

  }

}
