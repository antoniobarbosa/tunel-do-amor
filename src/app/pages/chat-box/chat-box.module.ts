import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ChatBoxPageRoutingModule } from './chat-box-routing.module';
import { ChatBoxPage } from './chat-box.page';
import { NgxAutoScrollModule } from 'ngx-auto-scroll';
import { FileServiceService } from 'src/app/services/file-service.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { Media, MediaObject } from '@ionic-native/media/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatBoxPageRoutingModule,
    NgxAutoScrollModule,
    ReactiveFormsModule,
    NgxIonicImageViewerModule,
  ],
  declarations: [ChatBoxPage],
  providers: [Media]
})
export class ChatBoxPageModule { }
